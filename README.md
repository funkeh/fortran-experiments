# FORTRAN Experiments
## by Josef Frank

Version used is FORTRAN 95

**Completed experiments**

1. *Hello World*  
  * Prints 'Hello World!'
2. *Reading Input*  
  * Calculates age of user based off of date of birth  
  * Also asks for and displays name of user  
3. *Trigonometry*  
  * Calculates sine of a user inputted angle  
4. *Choices*  
  * Implementing user choice  
5. *Loops*  
  * Working with various types of loops  
6. *Files*  
  * Reading and writing files  
7. *Arrays*  
  * Working with arrays  
8. *Vectors*  
  * Creating and allocating vectors