! Josef Frank :: 2015
! Work with choices


program choices

  implicit none
  
  real :: x, y, answer   ! Values set by program
  integer :: choice      ! Choice from user
  
  ! Give dummy values to x and y
  x = 5.5
  y = 9.0
  
  ! Print options menu
  print *, "Choose an option"
  print *, "1   Multiply"
  print *, "2   Divide"
  print *, "3   Add"
  
  ! Get option from user
  read *, choice
  
  ! Interpet user input
  if (choice == 1) then
    ! Multiply
    answer = x * y
  else if (choice == 2) then
    ! Divide
    answer = x / y
  else
    ! Add
    answer = x + y
  end if
  
  ! Output information to the user
  print *, "Result: ", answer
  
end program choices