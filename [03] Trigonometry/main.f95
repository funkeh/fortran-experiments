! Josef Frank :: 2015
! Find sine of angle in FORTRAN


program trig

  implicit none
  
  ! Angle is user inputted, pi is constant
  real :: angle, pi, sine
  
  ! Calculate pi
  pi = 4 * atan(1.0)
  
  ! Accept user input
  print *, "What angle would you like to find the sine of?"
  read *, angle
  
  ! Calculate sine of the angle
  sine = sin(angle * pi / 180)
  
  ! Output information to the user
  print *, "The sine of ", angle, " is ", sine, "."
  
end program trig