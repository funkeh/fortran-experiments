! Josef Frank :: 2015
! Managing arrays


program arrays

  implicit none

  integer, parameter :: array_size = 10   ! Size of array
  integer, dimension(array_size) :: x     ! Array of integers
  integer :: i      ! Counter for loops
  integer :: total  ! Sum of all array values
  
  
  ! Loop from 1 until array_size, incrementing by 1
  do i = 1, array_size
  
    ! Insert i * 2 into array at position i
    x(i) = i * 2
    
    ! Add x(i) to the total
    total = total + x(i)
    
  end do
  
  
  ! Tell the user what the average is
  print *, "The average of all values is: ", (total / array_size)
  
  ! Print all values of array
  print *, "The numbers are: ", x
 
end program arrays