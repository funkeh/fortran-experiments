! Josef Frank :: 2015
! Various types of loops


program loops

  implicit none
  
  integer :: i     ! Variable for loops
  
  ! Loop from 0 until 4 with a step increment of 2
  do i = 0, 4, 2
    
    print *, 'Increment loop value: ', i
    
  end do
  
  ! Loop from 20 until 0 with a step decrement of 5
  do i = 20, 0, -5
    
    print *, 'Decrement loop value: ', i
    
  end do
  
end program loops