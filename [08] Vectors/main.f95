! Josef Frank :: 2015
! Working with vectors
! NB. Vectors are arrays with no defined size or allocated memory


program vectors

  implicit none

  ! Array of unknown size with allocatable memory
  integer, allocatable, dimension(:) :: vector
  
  integer :: elements   ! Number of elements in vector
  integer :: i          ! Used for loops

  ! Ask user for number of elements in vector
  print *, "Please enter the number of elements in the vector."
  read *, elements
  
  ! Allocate memory of vector based on user input
  allocate(vector(elements))
  
  ! Provide feedback on vector size
  print *, "Your vector is of size ", elements, "."
  
  ! Ask for elements
  print *, "Please enter each element:"
  
  ! Take input for each element
  do i = 1, elements
  
    ! Read input
    read *, vector(i)
    
  end do
  
  ! Print out the vector
  print *, "This is your vector:"
  do i = 1, elements
    print *, vector(i)
  end do
  
  ! Tidy up memory
  deallocate(vector)
 
end program vectors