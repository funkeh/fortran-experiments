! Josef Frank :: 2015
! Reading input in FORTRAN


program input

  implicit none
  
  ! Calculated age of user and year in which they were born
  integer :: age, birth_year
  
  ! 20-length char of user's name 
  character :: name*20
  
  ! Accept input from the user and store it in variables
  print *, "What is your name?"
  read *, name
  print *, "What year were you born in?"
  read *, birth_year
  
  ! Figure out age of user
  age = 2015 - birth_year
  
  ! Print age
  print *, name, ", your age is ", age
  
end program input