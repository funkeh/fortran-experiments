! Josef Frank :: 2015
! Read from and write to files


program files

  implicit none
  
  ! Variables in which values from file will be stored
  real :: x, y, z
  
  ! Value for looping
  integer :: i
  
  ! Open reals.txt for reading
  open(10,  file = 'reals.txt')
  
  ! Read values from the file
  read(10, *) x, y, z
  
  ! Output the read values
  print *, "The values which have been read are: "
  print *, x
  print *, y
  print *, z
  
  ! Open ints.txt for writing
  open(11, file = 'ints.txt')
  
  ! Loop from 1 to 100
  do i = 1, 100
    
    ! Write to file from 0 to 198
    write(11, *) (i - 1) * 2
    
  end do
  
  ! Print successful writing message
  print *, "ints.txt created!"
 
end program files